package com.dnod.sauronteameye.ui

interface BackInterceptor {

    fun handleBackPress(): Boolean
}
