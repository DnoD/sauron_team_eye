package com.dnod.sauronteameye.ui

interface ScreenBuilderFactory<Screen> {

    fun create(screen: Screen): Conductor.ScreenBuilder<Screen>
}