package com.dnod.sauronteameye.ui.filter

import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.view.View
import com.dnod.sauronteameye.R
import com.dnod.sauronteameye.manager.FilterManager
import com.dnod.sauronteameye.provider.FilterProvider
import com.dnod.sauronteameye.ui.SingleEvent
import com.dnod.sauronteameye.ui.base.BaseViewModel
import com.dnod.sauronteameye.ui.getString

class FilterViewModel : BaseViewModel() {

    private lateinit var filterManager: FilterManager
    private val selectedProjects = hashSetOf<String>()
    private val selectedSkills = hashSetOf<String>()

    internal val choseProjectsAction = SingleEvent<Set<String>>()
    internal val choseSkillsAction = SingleEvent<Set<String>>()

    val useWorkingFilter = ObservableBoolean(false)
    val useHolidaysFilter = ObservableBoolean(false)
    val filterSkills = ObservableField<String>("")
    val filterProjects = ObservableField<String>("")

    fun start(filterManager: FilterManager, filterProvider: FilterProvider) {
        this.filterManager = filterManager
        useWorkingFilter.set(filterProvider.getFilter().working)
        useHolidaysFilter.set(filterProvider.getFilter().holidays)
        setProjects(filterProvider.getFilter().projects)
        setSkills(filterProvider.getFilter().skills)
    }

    fun onUseWorkingFilterChanged(view: View, checked: Boolean) {
        filterManager.setWorkingFilter(checked)
    }

    fun onUseHolidaysFilterChanged(view: View, checked: Boolean) {
        filterManager.setHolidaysFilter(checked)
    }

    fun choseProjects() {
        choseProjectsAction.postValue(selectedProjects)
    }

    fun choseSkills() {
        choseSkillsAction.postValue(selectedSkills)
    }

    fun setProjects(projects: Set<String>) {
        selectedProjects.clear()
        selectedProjects.addAll(projects)
        filterManager.setProjectFilter(selectedProjects)
        showProjects()
    }

    fun setSkills(skills: Set<String>) {
        selectedSkills.clear()
        selectedSkills.addAll(skills)
        filterManager.setSkillsFilter(selectedSkills)
        showSkills()
    }

    private fun showSkills() {
        if (selectedSkills.isEmpty()) {
            filterSkills.set(getString(R.string.main_screen_label_no_skills))
            return
        }
        filterSkills.set(getString(R.string.filter_screen_label_skills_format, selectedSkills.joinToString("; ") { skill -> skill }))
    }

    private fun showProjects() {
        if (selectedProjects.isEmpty()) {
            filterProjects.set(getString(R.string.main_screen_label_no_projects))
            return
        }
        filterProjects.set(getString(R.string.filter_screen_label_projects_format, selectedProjects.joinToString("; ") { project -> project }))
    }
}
