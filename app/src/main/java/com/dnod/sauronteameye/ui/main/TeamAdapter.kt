package com.dnod.sauronteameye.ui.main

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.dnod.sauronteameye.R
import com.dnod.sauronteameye.data.TeamMember
import com.dnod.sauronteameye.databinding.ItemTeamMemberBinding

class TeamAdapter(
    context: Context
) : RecyclerView.Adapter<TeamAdapter.Holder>() {

    init {
        setHasStableIds(true)
    }

    private val data: ArrayList<TeamMember> = ArrayList()
    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    fun addAll(data: List<TeamMember>) {
        val prevSize = this.data.size
        this.data.addAll(data)
        notifyItemRangeInserted(prevSize, data.size)
    }

    override fun getItemId(position: Int): Long {
        return data[position].id.toLong()
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): Holder {
        return Holder(DataBindingUtil.inflate(layoutInflater, R.layout.item_team_member, parent, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(data.get(position))
    }

    data class Holder(
        val binding: ItemTeamMemberBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.viewModel = TeamMemberViewModel()
        }

        fun bind(item: TeamMember) {
            binding.viewModel?.bind(item)
            binding.executePendingBindings()
        }
    }
}
