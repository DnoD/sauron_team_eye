package com.dnod.sauronteameye.ui.main

import android.databinding.ObservableField
import android.graphics.drawable.Drawable
import com.dnod.sauronteameye.R
import com.dnod.sauronteameye.data.*
import com.dnod.sauronteameye.ui.base.BaseViewModel
import com.dnod.sauronteameye.ui.getDrawable
import com.dnod.sauronteameye.ui.getString

class TeamMemberViewModel : BaseViewModel() {

    val nameAndProject = ObservableField<String>("")
    val skills = ObservableField<String>("")
    val statusIcon = ObservableField<Drawable>()

    fun bind(teamMember: TeamMember) {
        nameAndProject.set(teamMember.getFullName() + " - " + (teamMember.project?.name ?: "Unknown"))
        val skills = teamMember.getSkillsInline()
        if (skills.isNotEmpty()) {
            this.skills.set(skills)
        } else {
            this.skills.set(getString(R.string.main_screen_label_no_skills))
        }
        when(teamMember.getMemberStatus()) {
            Status.HOLIDAYS -> statusIcon.set(getDrawable(R.drawable.ic_vacation))
            Status.WORKING -> statusIcon.set(getDrawable(R.drawable.ic_work))
            Status.NOT_WORKING -> statusIcon.set(getDrawable(R.drawable.ic_home))
            Status.UNKNOWN -> statusIcon.set(getDrawable(R.drawable.ic_unknown))
        }
    }
}