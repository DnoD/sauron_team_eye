package com.dnod.sauronteameye.ui.main

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.view.MenuItem
import android.view.View
import com.dnod.sauronteameye.R
import com.dnod.sauronteameye.databinding.ActivityMainBinding
import com.dnod.sauronteameye.ui.Conductor
import com.dnod.sauronteameye.ui.ScreenBuilderFactory
import com.dnod.sauronteameye.ui.base.BaseActivity
import com.dnod.sauronteameye.ui.base.BaseFragment
import javax.inject.Inject

class MainActivity : BaseActivity(), Conductor<Conductor.ScreenBuilder<BaseFragment>> {

    private lateinit var bindingObject: ActivityMainBinding

    override val rootView: View
        get() = bindingObject.root

    private var currFragmentBuilder: Conductor.ScreenBuilder<BaseFragment>? = null

    @Inject
    lateinit var screenBuilderFactory: ScreenBuilderFactory<BaseFragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingObject = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setSupportActionBar(bindingObject.toolbar)
        goTo(screenBuilderFactory.create(MainFragment.createInstance()))
    }

    override fun goTo(builder: Conductor.ScreenBuilder<BaseFragment>) {
        val screen = builder.getScreen()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        if (builder.isRoot()) {
            //Clean back stack if any
            supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        } else {
            fragmentTransaction.addToBackStack(screen.getScreenTag())
        }
        currFragmentBuilder = builder
        fragmentTransaction.replace(bindingObject.container.id, screen, screen.getScreenTag())
                .commit()
        updateToolbarState(builder)
    }

    override fun goBack() {
        onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            goBack()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (currFragmentBuilder?.getScreen()?.handleBackPress() == true) {
            return
        }
        val prevIndex = supportFragmentManager.backStackEntryCount
        super.onBackPressed()
        if (prevIndex == 0) {
            return
        }
        postBackSetup()
        currFragmentBuilder?.let {
            updateToolbarState(it)
        }
    }

    private fun postBackSetup() {
        val stackFragment = supportFragmentManager.findFragmentById(bindingObject.container.getId()) as BaseFragment
        val stackBuilder = screenBuilderFactory.create(stackFragment)
        currFragmentBuilder = stackBuilder
    }

    private fun updateToolbarState(builder: Conductor.ScreenBuilder<BaseFragment>) {
        builder.getTitleResId()?.let {
            title = getString(it)
        }
        bindingObject.toolbar.title = title ?: ""
        supportActionBar?.setDisplayHomeAsUpEnabled(!builder.isRoot())
    }
}
