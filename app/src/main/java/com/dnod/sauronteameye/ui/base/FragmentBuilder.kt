package com.dnod.sauronteameye.ui.base

import com.dnod.sauronteameye.ui.Conductor

class FragmentBuilder(
    private val fragment: BaseFragment,
    private val titleResId: Int?
) : Conductor.ScreenBuilder<BaseFragment> {

    private var isRootScreen: Boolean = false

    fun setRootScreen() : FragmentBuilder {
        this.isRootScreen = true
        return this
    }

    override fun getTitleResId(): Int? {
        return titleResId
    }

    override fun getScreen(): BaseFragment {
        return fragment
    }

    override fun isRoot(): Boolean {
        return isRootScreen
    }
}