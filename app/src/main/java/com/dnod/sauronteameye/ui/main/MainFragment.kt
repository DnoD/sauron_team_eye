package com.dnod.sauronteameye.ui.main

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import com.dnod.sauronteameye.R
import com.dnod.sauronteameye.data.source.SauronEyeDataSource
import com.dnod.sauronteameye.databinding.FragmentMainBinding
import com.dnod.sauronteameye.ui.Conductor
import com.dnod.sauronteameye.ui.ScreenBuilderFactory
import com.dnod.sauronteameye.ui.base.BaseFragment
import com.dnod.sauronteameye.ui.filter.FilterFragment
import javax.inject.Inject

class MainFragment : BaseFragment() {

    companion object {
        private val TAG = MainFragment::class.java.simpleName

        fun createInstance(): BaseFragment {
            val fragment = MainFragment()
            val arguments = Bundle()
            fragment.arguments = arguments
            return fragment
        }
    }

    private lateinit var viewDataBinding: FragmentMainBinding

    @Inject
    lateinit var sauronEyeDataSource: SauronEyeDataSource

    @Inject
    lateinit var conductor: Conductor<Conductor.ScreenBuilder<BaseFragment>>

    @Inject
    lateinit var screenBuilderFactory: ScreenBuilderFactory<BaseFragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        viewDataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        viewDataBinding.apply {
            viewModel = ViewModelProviders.of(this@MainFragment).get(MainViewModel::class.java)
            setupTeamList()
            val nonOptionalViewModel = viewModel?.let { it } ?: return@apply
            nonOptionalViewModel.start(sauronEyeDataSource)
        }
        return viewDataBinding.root
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater) {
        inflater.inflate(R.menu.main, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.filter) {
            conductor.goTo(screenBuilderFactory.create(FilterFragment.createInstance()))
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun getScreenTag(): String {
        return TAG
    }

    private fun setupTeamList() {
        context?.let { it ->
            viewDataBinding.team.layoutManager = LinearLayoutManager(it)
            val dividerDecoration = DividerItemDecoration(it, DividerItemDecoration.VERTICAL)
            ContextCompat.getDrawable(it, R.drawable.shape_list_divider)?.let {
                dividerDecoration.setDrawable(it)
            }
            viewDataBinding.team.addItemDecoration(dividerDecoration)
            viewDataBinding.team.adapter = TeamAdapter(it)
        }
    }
}