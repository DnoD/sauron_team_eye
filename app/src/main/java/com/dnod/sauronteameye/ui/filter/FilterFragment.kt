package com.dnod.sauronteameye.ui.filter

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.DialogInterface
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dnod.sauronteameye.R
import com.dnod.sauronteameye.databinding.FragmentFilterBinding
import com.dnod.sauronteameye.manager.FilterManager
import com.dnod.sauronteameye.provider.FilterProvider
import com.dnod.sauronteameye.ui.base.BaseFragment
import javax.inject.Inject
import android.support.v7.app.AlertDialog
import com.dnod.sauronteameye.data.source.FilterDataSource

class FilterFragment : BaseFragment(), FilterDataSource.GetAvailableProjectsListener, FilterDataSource.GetAvailableSkillsListener {

    companion object {
        private val TAG = FilterFragment::class.java.simpleName

        fun createInstance(): BaseFragment {
            val fragment = FilterFragment()
            val arguments = Bundle()
            fragment.arguments = arguments
            return fragment
        }
    }

    private lateinit var viewDataBinding: FragmentFilterBinding
    private var availableSkills: List<String>? = null
    private var availableProjects: List<String>? = null
    private var selectedSkills = hashSetOf<String>()
    private var selectedProjects = hashSetOf<String>()

    @Inject
    lateinit var filterManager: FilterManager

    @Inject
    lateinit var filterProvider: FilterProvider

    @Inject
    lateinit var filterDataSource: FilterDataSource

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        viewDataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_filter, container, false)
        viewDataBinding.apply {
            viewModel = ViewModelProviders.of(this@FilterFragment).get(FilterViewModel::class.java)
            val nonOptionalViewModel = viewModel?.let { it } ?: return@apply
            nonOptionalViewModel.choseSkillsAction.observe(this@FilterFragment, Observer { skills ->
                skills?.let {
                    showSkillsDialog(it)
                }
            })
            nonOptionalViewModel.choseProjectsAction.observe(this@FilterFragment, Observer { projects ->
                projects?.let {
                    showProjectsDialog(it)
                }
            })
            nonOptionalViewModel.start(filterManager, filterProvider)
        }
        filterDataSource.getAvailableProjects(this)
        filterDataSource.getAvailableSkills(this)
        return viewDataBinding.root
    }

    override fun getScreenTag(): String {
        return TAG
    }

    override fun onReceiveSkills(skills: Set<String>) {
        availableSkills = ArrayList(skills)
    }

    override fun onReceiveSkillsFailure(error: String) {
    }

    override fun onReceiveProjects(projects: Set<String>) {
        availableProjects = ArrayList(projects)
    }

    override fun onReceiveProjectsFailure(error: String) {
    }

    private fun showSkillsDialog(selectedSkills: Set<String>) {
        val nonOptionalAvailableSkills = availableSkills ?: return
        this.selectedSkills.clear()
        this.selectedSkills.addAll(selectedSkills)
        context?.let {
            val builder = AlertDialog.Builder(it, R.style.AppAlertDialogStyle)
            builder.setMultiChoiceItems(
                    nonOptionalAvailableSkills.toTypedArray(),
                    nonOptionalAvailableSkills.map { skill -> selectedSkills.contains(skill) }.toBooleanArray()
            ) { _, which, isChecked ->
                if (isChecked) {
                    this.selectedSkills.add(nonOptionalAvailableSkills[which])
                } else {
                    this.selectedSkills.remove(nonOptionalAvailableSkills[which])
                }
            }.setPositiveButton(android.R.string.ok) { dialogInterface: DialogInterface, _: Int ->
                viewDataBinding.viewModel?.setSkills(this.selectedSkills)
                dialogInterface.dismiss()
            }
            builder.show()
        }
    }

    private fun showProjectsDialog(selectedProjects: Set<String>) {
        val nonOptionalAvailableProjects = availableProjects ?: return
        this.selectedProjects.clear()
        this.selectedProjects.addAll(selectedProjects)
        context?.let {
            val builder = AlertDialog.Builder(it, R.style.AppAlertDialogStyle)
            builder.setMultiChoiceItems(
                    nonOptionalAvailableProjects.toTypedArray(),
                    nonOptionalAvailableProjects.map { project -> selectedProjects.contains(project) }.toBooleanArray()
            ) { _, which, isChecked ->
                if (isChecked) {
                    this.selectedProjects.add(nonOptionalAvailableProjects[which])
                } else {
                    this.selectedProjects.remove(nonOptionalAvailableProjects[which])
                }
            }.setPositiveButton(android.R.string.ok) { dialogInterface: DialogInterface, _: Int ->
                viewDataBinding.viewModel?.setProjects(this.selectedProjects)
                dialogInterface.dismiss()
            }
            builder.show()
        }
    }
}