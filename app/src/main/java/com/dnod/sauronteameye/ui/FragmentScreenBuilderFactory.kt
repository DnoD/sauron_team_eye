package com.dnod.sauronteameye.ui

import com.dnod.sauronteameye.R
import com.dnod.sauronteameye.ui.base.BaseFragment
import com.dnod.sauronteameye.ui.base.FragmentBuilder
import com.dnod.sauronteameye.ui.filter.FilterFragment
import com.dnod.sauronteameye.ui.main.MainFragment
import javax.inject.Inject

/**
 * This is the Factory which is dedicated in create the Concrete Builder of the Screen which is going to be attached using Fragments
 */
class FragmentScreenBuilderFactory @Inject constructor() : ScreenBuilderFactory<BaseFragment> {

    override fun create(screen: BaseFragment): Conductor.ScreenBuilder<BaseFragment> {
        return when (screen) {
            is MainFragment -> FragmentBuilder(screen, R.string.main_screen_title)
                .setRootScreen()
            is FilterFragment -> FragmentBuilder(screen, R.string.filter_screen_title)
            else -> throw UnsupportedOperationException()
        }
    }

}