package com.dnod.sauronteameye.ui

import android.graphics.drawable.Drawable
import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import android.support.v4.content.ContextCompat
import com.dnod.sauronteameye.TeamEyeApplication
import com.dnod.sauronteameye.ui.base.BaseViewModel

fun BaseViewModel.getDrawable(@DrawableRes drawableRes: Int): Drawable? {
    return ContextCompat.getDrawable(TeamEyeApplication.instance, drawableRes)
}

fun BaseViewModel.getString(@StringRes stringResRes: Int): String {
    return TeamEyeApplication.instance.getString(stringResRes)
}

fun BaseViewModel.getString(@StringRes stringRes: Int, vararg args: String): String {
    return TeamEyeApplication.instance.getString(stringRes, *args)
}