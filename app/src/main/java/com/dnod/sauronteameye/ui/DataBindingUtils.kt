package com.dnod.sauronteameye.ui

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import com.dnod.sauronteameye.data.TeamMember
import com.dnod.sauronteameye.ui.main.TeamAdapter
import com.dnod.sauronteameye.ui.view.PaginateRecycleView
import com.paginate.Paginate

@BindingAdapter("data")
fun <T> setRecyclerViewProperties(recyclerView: RecyclerView, items: List<*>?) {
    items?.let {
        var adapter = recyclerView.adapter
        if (adapter is TeamAdapter) {
            adapter.addAll(items as List<TeamMember>)
        }
    }
}

@BindingAdapter("paginateListener")
fun <T> setPaginateListener(recyclerView: PaginateRecycleView, listener: Paginate.Callbacks?) {
    listener?.let {
        recyclerView.setPaginateListener(it)
    }
}