package com.dnod.sauronteameye.ui

import android.support.annotation.StringRes

interface Conductor<ScreenBuilder> {

    interface ScreenBuilder<Screen> {

        fun getScreen(): Screen

        @StringRes
        fun getTitleResId(): Int?

        fun isRoot(): Boolean
    }

    fun goTo(builder: ScreenBuilder)

    fun goBack()
}
