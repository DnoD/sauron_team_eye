package com.dnod.sauronteameye.ui.main

import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import com.dnod.sauronteameye.data.TeamMember
import com.dnod.sauronteameye.data.source.SauronEyeDataSource
import com.paginate.Paginate

class MainViewModel : ViewModel(), SauronEyeDataSource.GetTeamListener, Paginate.Callbacks {

    private lateinit var sauronEyeDataSource: SauronEyeDataSource
    private var hasNextPage = false
    private var isNextPageLoading = false

    val isDataAvailable = ObservableBoolean(false)
    val isDataLoading = ObservableBoolean(false)
    val isErrorOccurred = ObservableBoolean(false)
    val team = ObservableField<List<TeamMember>>()
    val paginateListener = ObservableField<Paginate.Callbacks>()

    fun start(sauronEyeDataSource: SauronEyeDataSource) {
        this.sauronEyeDataSource = sauronEyeDataSource
        isDataLoading.set(true)
        isDataAvailable.set(true)
        team.set(emptyList())
        sauronEyeDataSource.getTeam(this)
    }

    override fun onReceiveTeam(team: List<TeamMember>, hasNextPage: Boolean) {
        isNextPageLoading = false
        this.hasNextPage = hasNextPage
        isDataAvailable.set(!team.isEmpty())
        isDataLoading.set(false)
        this.team.set(team)
        paginateListener.set(this)
    }

    override fun onReceiveTeamFailure(error: String) {
        isErrorOccurred.set(true)
        isDataLoading.set(false)
    }

    override fun onLoadMore() {
        isNextPageLoading = true
        sauronEyeDataSource.getNextPage(this)
    }

    override fun isLoading(): Boolean {
        return isNextPageLoading
    }

    override fun hasLoadedAllItems(): Boolean {
        return !hasNextPage
    }
}
