package com.dnod.sauronteameye.utils

import android.annotation.SuppressLint
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.TimeZone

object DateFormatUtil {

    @SuppressLint("SimpleDateFormat")
    fun getDateFromTimeString(dateString: String): Date? {
        val format = SimpleDateFormat("HH:mm")
        var date: Date? = null
        try {
            date = format.parse(dateString)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return date
    }
}
