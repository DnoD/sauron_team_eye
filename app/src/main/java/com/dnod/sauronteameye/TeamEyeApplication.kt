package com.dnod.sauronteameye

import com.dnod.sauronteameye.di.component.ApplicationComponent
import com.dnod.sauronteameye.di.component.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class TeamEyeApplication : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication>? {
        appComponent = DaggerApplicationComponent.builder().application(this).build()
        appComponent?.inject(this)
        return appComponent
    }

    companion object {

        lateinit var instance: TeamEyeApplication

        var appComponent: ApplicationComponent? = null
            get
    }
}
