package com.dnod.sauronteameye.di.module

import com.dnod.sauronteameye.data.source.FilterDataSource
import com.dnod.sauronteameye.data.source.FilterRepository
import com.dnod.sauronteameye.data.source.SauronEyeDataSource
import com.dnod.sauronteameye.data.source.SauronEyeRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
abstract class DataSourceModule {

    @Module
    companion object {

        @JvmStatic
        @Provides
        internal fun provideSauronEyeRepository(dataSource: SauronEyeRepository): SauronEyeDataSource {
            return dataSource
        }

        @JvmStatic
        @Provides
        @Singleton
        internal fun provideFiltersRepository(dataSource: FilterRepository): FilterDataSource {
            return dataSource
        }
    }

}
