package com.dnod.sauronteameye.di.module

import com.dnod.sauronteameye.di.scope.ActivityScoped
import com.dnod.sauronteameye.di.scope.FragmentScoped
import com.dnod.sauronteameye.ui.Conductor
import com.dnod.sauronteameye.ui.FragmentScreenBuilderFactory
import com.dnod.sauronteameye.ui.ScreenBuilderFactory
import com.dnod.sauronteameye.ui.base.BaseFragment
import com.dnod.sauronteameye.ui.filter.FilterFragment
import com.dnod.sauronteameye.ui.main.MainActivity
import com.dnod.sauronteameye.ui.main.MainFragment
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun addMainFragment(): MainFragment

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun addFilterFragment(): FilterFragment

    @Module
    companion object {

        @JvmStatic
        @Provides
        @ActivityScoped
        internal fun provideScreenBuilderFactory(factory: FragmentScreenBuilderFactory): ScreenBuilderFactory<BaseFragment> {
            return factory
        }

        @JvmStatic
        @Provides
        @ActivityScoped
        internal fun provideConductor(activity: MainActivity): Conductor<Conductor.ScreenBuilder<BaseFragment>> {
            return activity
        }
    }

}
