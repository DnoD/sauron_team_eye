package com.dnod.sauronteameye.di.module

import com.dnod.sauronteameye.manager.FilterManager
import com.dnod.sauronteameye.manager.FilterManagerImpl
import com.dnod.sauronteameye.provider.FilterProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
abstract class FilterModule {

    @Module
    companion object {

        @JvmStatic
        @Provides
        @Singleton
        internal fun provideFilterManagerInstance(): FilterManagerImpl {
            return FilterManagerImpl()
        }

        @JvmStatic
        @Provides
        @Singleton
        internal fun provideFilterManager(manager: FilterManagerImpl): FilterManager {
            return manager
        }

        @JvmStatic
        @Provides
        @Singleton
        internal fun provideFilterProvider(manager: FilterManagerImpl): FilterProvider {
            return manager
        }

    }

}
