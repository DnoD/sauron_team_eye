package com.dnod.sauronteameye.di.module

import com.dnod.sauronteameye.ui.main.MainActivity
import com.dnod.sauronteameye.di.scope.ActivityScoped
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = [
        MainActivityModule::class
    ])
    abstract fun mainActivity(): MainActivity
}
