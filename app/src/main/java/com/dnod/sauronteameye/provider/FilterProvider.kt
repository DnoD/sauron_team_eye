package com.dnod.sauronteameye.provider

import com.dnod.sauronteameye.data.Filter

interface FilterProvider {

    fun getFilter() : Filter
}