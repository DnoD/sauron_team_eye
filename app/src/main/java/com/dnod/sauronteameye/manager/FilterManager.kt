package com.dnod.sauronteameye.manager

interface FilterManager {

    fun setSkillsFilter(skills: Set<String>)

    fun setWorkingFilter(useWorking: Boolean)

    fun setHolidaysFilter(useHolidays: Boolean)

    fun setProjectFilter(projects: Set<String>)
}