package com.dnod.sauronteameye.manager

import com.dnod.sauronteameye.data.Filter
import com.dnod.sauronteameye.provider.FilterProvider
import javax.inject.Inject

class FilterManagerImpl @Inject constructor(): FilterManager, FilterProvider {

    private val filter = Filter()

    override fun setSkillsFilter(skills: Set<String>) {
        filter.skills = skills
    }

    override fun setWorkingFilter(useWorking: Boolean) {
        filter.working = useWorking
    }

    override fun setHolidaysFilter(useHolidays: Boolean) {
        filter.holidays = useHolidays
    }

    override fun setProjectFilter(projects: Set<String>) {
        filter.projects = projects
    }

    override fun getFilter(): Filter {
        return filter
    }
}