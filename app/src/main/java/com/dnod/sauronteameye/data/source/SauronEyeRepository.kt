package com.dnod.sauronteameye.data.source

import com.dnod.sauronteameye.data.source.remote.ClientApi
import com.dnod.sauronteameye.provider.FilterProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SauronEyeRepository @Inject constructor(
    private val clientApi: ClientApi,
    private val filterProvider: FilterProvider
) : SauronEyeDataSource {

    private val composite = CompositeDisposable()
    private var nextPage: Int? = 0

    override fun getTeam(listener: SauronEyeDataSource.GetTeamListener) {
        composite.clear()
        composite.add(clientApi.getTeam(1, filterProvider.getFilter())
            .delay(1000, TimeUnit.MILLISECONDS)  //Small delay for the request sending imitation
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response ->
                nextPage = if (response.hasNext) response.page else null
                listener.onReceiveTeam(response.items, response.hasNext)
            }) { throwable ->
                listener.onReceiveTeamFailure(throwable.localizedMessage)
            })
    }

    override fun getNextPage(listener: SauronEyeDataSource.GetTeamListener) {
        nextPage?.let {
            composite.add(clientApi.getTeam(it, filterProvider.getFilter())
                .delay(1000, TimeUnit.MILLISECONDS)  //Small delay for the request sending imitation
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    nextPage = if (response.hasNext) response.page else null
                    listener.onReceiveTeam(response.items, response.hasNext)
                }) { throwable ->
                    listener.onReceiveTeamFailure(throwable.localizedMessage)
                })
        }
    }
}