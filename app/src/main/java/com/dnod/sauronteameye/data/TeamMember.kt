package com.dnod.sauronteameye.data

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.util.*

data class TeamMember(
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("first_name")
    var firstName: String = "",
    @SerializedName("last_name")
    var lastName: String = "",
    @SerializedName("on_holidays_till")
    var onHolidaysTill: Date = Date(),
    @SerializedName("free_since")
    var free_since: Date = Date(),
    @SerializedName("current_project")
    var project: Project? = null,
    @SerializedName("working_hours")
    var workingHours: WorkingHours? = null,
    @SerializedName("skills")
    var skills: List<String> = emptyList()
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readSerializable() as Date,
        parcel.readSerializable() as Date,
        parcel.readParcelable(Project::class.java.classLoader),
        parcel.readParcelable(WorkingHours::class.java.classLoader),
        parcel.createStringArrayList() ?: emptyList()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(firstName)
        parcel.writeString(lastName)
        parcel.writeSerializable(onHolidaysTill)
        parcel.writeSerializable(free_since)
        parcel.writeParcelable(project, flags)
        parcel.writeParcelable(workingHours, flags)
        parcel.writeStringList(skills)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TeamMember> {
        override fun createFromParcel(parcel: Parcel): TeamMember {
            return TeamMember(parcel)
        }

        override fun newArray(size: Int): Array<TeamMember?> {
            return arrayOfNulls(size)
        }
    }

    data class WorkingHours(
        @SerializedName("timezone")
        var timezone: String,
        @SerializedName("end")
        var end: String,
        @SerializedName("start")
        var start: String
    ) : Parcelable {
        constructor(parcel: Parcel) : this(
            parcel.readString() ?: Calendar.getInstance().timeZone.displayName,
            parcel.readString() ?: "",
            parcel.readString() ?: ""
        )

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeString(timezone)
            parcel.writeString(end)
            parcel.writeString(start)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<WorkingHours> {
            override fun createFromParcel(parcel: Parcel): WorkingHours {
                return WorkingHours(parcel)
            }

            override fun newArray(size: Int): Array<WorkingHours?> {
                return arrayOfNulls(size)
            }
        }

    }
}