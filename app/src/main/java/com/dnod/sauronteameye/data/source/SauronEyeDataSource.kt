package com.dnod.sauronteameye.data.source

import com.dnod.sauronteameye.data.TeamMember

interface SauronEyeDataSource {

    interface GetTeamListener {
        fun onReceiveTeam(team: List<TeamMember>, hasNextPage: Boolean)

        fun onReceiveTeamFailure(error: String)
    }

    fun getTeam(listener: GetTeamListener)

    fun getNextPage(listener: GetTeamListener)
}