package com.dnod.sauronteameye.data.source.remote

import com.dnod.sauronteameye.data.source.remote.response.TeamResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface SauronEyeService {

    @GET("/team")
    fun getTeam(
            @Query("page") page: Int,
            @Query("holidays") holidays: Boolean?,
            @Query("working") working: Boolean?,
            @Query("project") projects: Set<String>,
            @Query("skill") skill: Set<String>
    ): Observable<Response<TeamResponse>>
}