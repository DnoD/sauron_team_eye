package com.dnod.sauronteameye.data.source.remote

import com.dnod.sauronteameye.data.Filter
import com.dnod.sauronteameye.data.source.remote.response.TeamResponse
import io.reactivex.Observable

interface ClientApi {

    fun getTeam(page: Int, filter: Filter) : Observable<TeamResponse>
}