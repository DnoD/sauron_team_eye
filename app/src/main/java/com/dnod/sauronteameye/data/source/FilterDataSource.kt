package com.dnod.sauronteameye.data.source

interface FilterDataSource {

    interface GetAvailableSkillsListener {
        fun onReceiveSkills(skills: Set<String>)

        fun onReceiveSkillsFailure(error: String)
    }

    interface GetAvailableProjectsListener {
        fun onReceiveProjects(projects: Set<String>)

        fun onReceiveProjectsFailure(error: String)
    }

    fun getAvailableSkills(listener: GetAvailableSkillsListener)

    fun getAvailableProjects(listener: GetAvailableProjectsListener)
}