package com.dnod.sauronteameye.data

enum class Status {
    WORKING, HOLIDAYS, NOT_WORKING, UNKNOWN
}