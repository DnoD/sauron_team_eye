package com.dnod.sauronteameye.data.source

import javax.inject.Inject

class FilterRepository @Inject constructor(
) : FilterDataSource {

    private val AVAILABLE_PROJECTS = hashSetOf("11", "55")
    private val AVAILABLE_SKILLS = hashSetOf("js", "Angular", "python", "JAVA", "C++")

    override fun getAvailableProjects(listener: FilterDataSource.GetAvailableProjectsListener) {
        listener.onReceiveProjects(AVAILABLE_PROJECTS)
    }

    override fun getAvailableSkills(listener: FilterDataSource.GetAvailableSkillsListener) {
        listener.onReceiveSkills(AVAILABLE_SKILLS)
    }
}