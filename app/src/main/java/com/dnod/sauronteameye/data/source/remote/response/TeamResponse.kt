package com.dnod.sauronteameye.data.source.remote.response

import com.dnod.sauronteameye.data.TeamMember
import com.google.gson.annotations.SerializedName

data class TeamResponse(
    @SerializedName("items")
    var items: List<TeamMember> = emptyList(),
    @SerializedName("has_previous")
    var hasPrevious: Boolean = false,
    @SerializedName("has_next")
    var hasNext: Boolean = false,
    @SerializedName("page")
    var page: Int = 0
) {
}