package com.dnod.sauronteameye.data.source.remote

import android.content.Context
import com.dnod.sauronteameye.BuildConfig
import com.dnod.sauronteameye.R
import com.dnod.sauronteameye.data.Filter
import com.dnod.sauronteameye.data.Status
import com.dnod.sauronteameye.data.getMemberStatus
import com.dnod.sauronteameye.data.source.remote.response.TeamResponse
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.lang.Exception
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import okhttp3.OkHttpClient

/**
 * This is a concrete ClientApi using Retrofit2 library for the connection with server
 */
class RetrofitClientApi @Inject constructor(
        @Endpoint val endpointUrl: String,
        @ConnectionTimeout val connectionTimeout: Long,
        @ConnectionRetryAttempts val connectionRetryAttempts: Int,
        private val context: Context
) : ClientApi {

    private val sauronEyeService: SauronEyeService

    init {
        val okHttpBuilder = OkHttpClient.Builder()
                .cache(null)
                .connectTimeout(connectionTimeout, TimeUnit.MILLISECONDS)
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        if (BuildConfig.BUILD_TYPE == "debug") {
            okHttpBuilder.addInterceptor(MockApiInterceptor())
        } else {
            okHttpBuilder.addInterceptor(object : Interceptor {
                @Throws(IOException::class)
                override fun intercept(chain: Interceptor.Chain): okhttp3.Response? {
                    val request: okhttp3.Request = chain.request()
                    return proceed(chain, request, 0) ?: return null
                }

                @Throws(IOException::class)
                private fun proceed(
                        chain: Interceptor.Chain,
                        request: okhttp3.Request,
                        retryCount: Int
                ): okhttp3.Response? {
                    var count = retryCount
                    val response: okhttp3.Response?
                    try {
                        response = chain.proceed(request)
                    } catch (e: IOException) {
                        return if (retryCount < connectionRetryAttempts) {
                            proceed(chain, request, ++count)
                        } else {
                            throw e
                        }
                    }

                    return response
                }
            })
        }
        val retrofitClient = Retrofit.Builder()
                .baseUrl(endpointUrl)
                .client(okHttpBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
        sauronEyeService = retrofitClient.create(SauronEyeService::class.java)
    }

    override fun getTeam(page: Int, filter: Filter): Observable<TeamResponse> {
        return sauronEyeService.getTeam(page, if (filter.holidays) true else null, if (filter.working) true else null, filter.projects, filter.skills)
                .subscribeOn(Schedulers.io())
                .map { response ->
                    if (response.errorBody() == null) {
                        val body = response.body()
                                ?: throw Exception(context.getString(R.string.common_error_messages_unexpected_server_response))
                        if (BuildConfig.BUILD_TYPE == "debug" && isFilterApplied(filter)) {
                            body.items = body.items.filter {
                                val status = it.getMemberStatus()
                                return@filter ((filter.holidays && status == Status.HOLIDAYS ||
                                        filter.working && status == Status.WORKING ||
                                        (!filter.holidays && !filter.working)) &&
                                        (filter.projects.isEmpty() || filter.projects.contains(it.project?.id)) &&
                                        (filter.skills.isEmpty() || it.skills.any { skill -> filter.skills.contains(skill) }))
                            }
                        }
                        body
                    } else {
                        throw Exception(context.getString(R.string.common_error_messages_unexpected_server_response))
                    }
                }
    }

    private fun isFilterApplied(filter: Filter): Boolean {
        return (filter.holidays || filter.working || filter.projects.isNotEmpty() || filter.skills.isNotEmpty())
    }
}