package com.dnod.sauronteameye.data

data class Filter(
    var holidays: Boolean = false,
    var working: Boolean = false,
    var projects: Set<String> = emptySet(),
    var skills: Set<String> = emptySet()
)