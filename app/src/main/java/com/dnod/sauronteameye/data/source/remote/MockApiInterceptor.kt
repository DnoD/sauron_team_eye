package com.dnod.sauronteameye.data.source.remote

import com.dnod.sauronteameye.R
import com.dnod.sauronteameye.TeamEyeApplication
import okhttp3.*
import java.io.IOException

class MockApiInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val responseString: String
        val request = chain.request()
        if (request.url().queryParameterNames().contains("skill") ||
            request.url().queryParameterNames().contains("project") ||
            request.url().queryParameterNames().contains("holidays") ||
            request.url().queryParameterNames().contains("working")) {
            responseString = TeamEyeApplication.instance.resources.openRawResource(R.raw.full_result).bufferedReader().use { it.readText() }
        } else if (request.url().queryParameter("page") == "1") {
            responseString = TeamEyeApplication.instance.resources.openRawResource(R.raw.result_page_1).bufferedReader().use { it.readText() }
        } else {
            responseString = TeamEyeApplication.instance.resources.openRawResource(R.raw.result_page_2).bufferedReader().use { it.readText() }
        }
        return Response.Builder()
            .code(200)
            .message(responseString)
            .request(chain.request())
            .protocol(Protocol.HTTP_1_0)
            .body(ResponseBody.create(MediaType.parse("application/json"), responseString.toByteArray()))
            .addHeader("content-type", "application/json")
            .build()
    }
}