package com.dnod.sauronteameye.data

import com.dnod.sauronteameye.utils.DateFormatUtil
import java.util.*

fun TeamMember.getMemberStatus(): Status {
    val now = Calendar.getInstance()
    if (onHolidaysTill.after(now.time)) {
        return Status.HOLIDAYS
    }
    var isWorking = false
    DateFormatUtil.getDateFromTimeString(workingHours?.start ?: "").let {
        val startTime = Calendar.getInstance()
        startTime.time = it
        isWorking =
            now.get(Calendar.HOUR_OF_DAY) > startTime.get(Calendar.HOUR_OF_DAY) && now.get(Calendar.MINUTE) > startTime.get(
                Calendar.MINUTE
            )
    }
    if (isWorking) {
        DateFormatUtil.getDateFromTimeString(workingHours?.end ?: "").let {
            val endTime = Calendar.getInstance()
            endTime.time = it
            isWorking = (now.get(Calendar.HOUR_OF_DAY) < endTime.get(Calendar.HOUR_OF_DAY)) ||
                    (now.get(Calendar.HOUR_OF_DAY) == endTime.get(Calendar.HOUR_OF_DAY) && now.get(Calendar.MINUTE) < endTime.get(Calendar.MINUTE))
        }
    }
    return if (isWorking) Status.WORKING else Status.NOT_WORKING
}

fun TeamMember.getFullName(): String {
    return "$firstName $lastName"
}

fun TeamMember.getSkillsInline(): String {
    return skills.joinToString("; ") { skill -> skill }
}